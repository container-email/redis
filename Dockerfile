ARG DVER=latest
FROM docker.io/alpine:$DVER
LABEL maintainer="Duncan Bellamy <dunk@denkimushi.com>"
ARG APKVER

RUN apk update \
&& apk upgrade --available --no-cache \
&& apk add --no-cache --upgrade openssl redis$APKVER stunnel

COPY --chmod=644 redis.conf /etc/redis.conf
COPY --chmod=644 stunnel.conf /etc/stunnel/stunnel.conf

WORKDIR /usr/local/bin
COPY --chmod=755 container-scripts/set-timezone.sh entrypoint.sh ./
WORKDIR /data

CMD [ "entrypoint.sh" ]

HEALTHCHECK --start-period=60s CMD redis-cli PING || exit 1
